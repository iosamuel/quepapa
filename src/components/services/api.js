var API = function() {
	this.url = 'https://que-papa.herokuapp.com/'
}

API.prototype.login = function(params, cb) {
	$.post(this.url+'login', params, cb);
};

API.prototype.get = function(s, params, cb) {
	$.get(this.url+s, params, cb);
};

API.prototype.post = function(s, params, cb) {
	$.post(this.url+s, params, cb);
};

module.exports = new API();
