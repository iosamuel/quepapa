import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'

/*
 *	Components
 */
let Home = require('components/Home.vue');
let Order = require('components/Order.vue');
let Login = require('components/Login.vue');
let Error404 = require('components/404.vue');
let ErrorMessage = require('components/ErrorMessage.vue');

// Installation
Vue.use(VueRouter);

require("!style!css!sass!./sass/main.scss");

/*
 *	Routes
 */
const router = new VueRouter({
	mode: 'history',
	routes: [
		{path: '/', name: 'home', component: Home},
		{path: '/order', name: 'order', component: Order},
		{path: '/login', name: 'login', component: Login},
		{path: '/error/:message', name: 'error', component: ErrorMessage},
		{path: '*', name: '404', component: ErrorMessage}
	]
});

/* Main App Initialization */
new Vue({
	router,
	el: '#app',
	render: h => h(App)
});
